# OpenML dataset: Cosmetics-datasets

https://www.openml.org/d/43481

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Whenever I want to try a new cosmetic item, it's so difficult to choose. It's actually more than difficult. It's sometimes scary because new items that I've never tried end up giving me skin trouble. We know the information we need is on the back of each product, but it's really hard to interpret those ingredient lists unless you're a chemist. You may be able to relate to this situation.

Content
we are going to create a content-based recommendation system where the 'content' will be the chemical components of cosmetics. Specifically, we will process ingredient lists for 1472 cosmetics on Sephora via word embedding, then visualize ingredient similarity using a machine learning method called t-SNE and an interactive visualization library called Bokeh. Let's inspect our data first.

Acknowledgements
DataCamp

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43481) of an [OpenML dataset](https://www.openml.org/d/43481). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43481/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43481/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43481/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

